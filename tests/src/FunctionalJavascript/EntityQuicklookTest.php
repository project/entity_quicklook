<?php

namespace Drupal\FunctionalTests\entity_quicklook;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;

/**
 * @group entity_quicklook
 */
class EntityQuicklookTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'entity_quicklook',
  ];

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $article;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $page;

  protected function setUp() {
    parent::setUp();
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article'
    ]);

    FieldStorageConfig::create([
      'field_name' => 'test_field',
      'entity_type' => 'node',
      'translatable' => FALSE,
      'entity_types' => [],
      'settings' => [
        'target_type' => 'node',
      ],
      'type' => 'entity_reference',
      'cardinality' => 1,
    ])->save();

    FieldConfig::create([
      'label' => 'Entity reference field',
      'field_name' => 'test_field',
      'entity_type' => 'node',
      'bundle' => 'page',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'article',
          ],
        ],
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    $display_repository->getViewDisplay('node', 'page')
      ->setComponent('test_field', [
        'type' => 'entity_quicklook_formatter',
      ])
      ->save();

    $this->article = Node::create([
      'title' => 'An article on Quicklook',
      'type' => 'article',
      'body' => [
        'value' => 'Test article body for Quicklook testing.',
      ],
    ]);
    $this->article->save();


    $this->page = Node::create([
      'title' => 'Test page',
      'type' => 'page',
      'body' => [
        'value' => 'Test page related to an article',
      ],
      'test_field' => $this->article->id(),
    ]);
    $this->page->save();
  }

  public function testIt() {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    $this->drupalLogin($this->createUser(['access content']));
    $this->drupalGet($this->page->toUrl());
    $assert->pageTextNotContains('Test article body for Quicklook testing.');
    $page->clickLink('An article on Quicklook');
    $this->assertNotEmpty($assert->waitForText('Test article body for Quicklook testing.'));
    $page->pressButton('Close');
    $assert->pageTextNotContains('Test article body for Quicklook testing.');
  }

}
