<?php

namespace Drupal\FunctionalTests\entity_quicklook;

use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\Tests\BrowserTestBase;

/**
 * @group entity_quicklook
 */
class EntityQuicklookAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'entity_quicklook',
  ];

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $article;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $page;

  protected function setUp() {
    parent::setUp();
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);
    $this->drupalCreateContentType([
      'type' => 'article',
      'name' => 'Article'
    ]);

    FieldStorageConfig::create([
      'field_name' => 'test_field',
      'entity_type' => 'node',
      'translatable' => FALSE,
      'entity_types' => [],
      'settings' => [
        'target_type' => 'node',
      ],
      'type' => 'entity_reference',
      'cardinality' => 1,
    ])->save();

    FieldConfig::create([
      'label' => 'Entity reference field',
      'field_name' => 'test_field',
      'entity_type' => 'node',
      'bundle' => 'page',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'article',
          ],
        ],
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    $display_repository->getViewDisplay('node', 'page')
      ->setComponent('test_field', [
        'type' => 'entity_quicklook_formatter',
      ])
      ->save();

    $this->article = Node::create([
      'title' => 'An article for Quicklook',
      'type' => 'article',
      'body' => [
        'value' => 'Test article body for Quicklook testing.',
      ],
    ]);
    $this->article->save();


    $this->page = Node::create([
      'title' => 'Test page',
      'type' => 'page',
      'body' => [
        'value' => 'Test page related to an article',
      ],
      'test_field' => $this->article->id(),
    ]);
    $this->page->save();
  }

  /**
   * Tests that the entity_quicklook_formatter.render_popup route does not leak.
   */
  public function testRenderRoute() {
    $assert = $this->assertSession();
    $this->drupalLogin($this->createUser(['access content']));

    $parameters = [
      'parent_entity_type' => 'node',
      'parent_entity' => $this->page->id(),
      'from_view' => 'default',
      'entity_type' => 'node',
      'entity' => $this->article->id(),
      'view_mode' => 'default',
    ];
    $url = Url::fromRoute('entity_quicklook_formatter.render_popup', $parameters);
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('Test article body for Quicklook testing.');

    // Unpublish the referenced node.
    $this->article->setUnpublished()->save();
    $this->drupalGet($url);
    $assert->statusCodeEquals(403);

    // Publish the referenced node and unpublish the referencing node.
    $this->article->setPublished()->save();
    $this->page->setUnpublished()->save();
    $this->drupalGet($url);
    $assert->statusCodeEquals(403);

    // Unpublish both nodes.
    $this->article->setUnpublished()->save();
    $this->drupalGet($url);
    $assert->statusCodeEquals(403);

    // Publish both but change the formatter to a label.
    $this->article->setPublished()->save();
    $this->page->setPublished()->save();
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getViewDisplay('node', 'page')
      ->setComponent('test_field', [
        'type' => 'entity_reference_label',
      ])
      ->save();
    $this->drupalGet($url);
    $assert->statusCodeEquals(403);

    // Change the formatter back and access should be allowed again.
    $display_repository->getViewDisplay('node', 'page')
      ->setComponent('test_field', [
        'type' => 'entity_quicklook_formatter',
      ])
      ->save();
    $this->drupalGet($url);
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('Test article body for Quicklook testing.');
  }

  /**
   * Tests what happens when a formatter references unpublished nodes.
   */
  public function testFormatter() {
    $assert = $this->assertSession();

    $this->drupalLogin($this->createUser(['access content']));
    $this->drupalGet($this->page->toUrl());
    $assert->pageTextContains('An article for Quicklook');

    // Unpublish the referenced node.
    $this->article->setUnpublished()->save();
    $this->drupalGet($this->page->toUrl());
    $assert->pageTextNotContains('An article for Quicklook');

    // @todo So the good thing is that the quicklook formatter is not showing
    //   unpublished node titles but the weird thing is that the code in
    //   EntityQuicklookFormatter::viewElements() creates a reference to self as
    //   there are no entities to view. This behaviour seems bizarre.
  }

}
